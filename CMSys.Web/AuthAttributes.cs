﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CMSys.Web
{
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        public AuthorizeAttribute(params string[] claim) : base(typeof(AuthorizeFilter))
        {
            Arguments = new object[] {claim};
        }
    }

    public class AuthorizeFilter : IAuthorizationFilter
    {
        private readonly string[] _claim;

        public AuthorizeFilter(params string[] claim)
        {
            _claim = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var IsAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
            var claimsIndentity = context.HttpContext.User.Identity as ClaimsIdentity;

            if (IsAuthenticated)
            {
                var flagClaim = false;
                foreach (var item in _claim)
                    if (context.HttpContext.User.HasClaim(
                        "http://schemas.microsoft.com/ws/2008/06/identity/claims/role", item))
                        flagClaim = true;
                if (!flagClaim)
                    context.Result = new RedirectResult("~/Account/Login");
            }
            else
            {
                context.Result = new RedirectResult("~/Account/Login");
            }
        }
    }
}
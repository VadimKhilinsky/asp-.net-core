﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace CMSys.Web
{
    public class AuthOptions
    {
        public const string ISSUER = "http://localhost:58961";
        public const string AUDIENCE = "http://localhost:58961";
        private const string KEY = "coherent_solutions_key_101";
        public const int LIFETIME = 1;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
﻿using System;
using System.Linq;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class TrainerController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        private void AddTrainerGroupsToView()
        {
            ViewBag.Groups = _uow.TrainerGroupRepository.List();
        }

        [Route("admin/trainers")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("admin/trainers/create")]
        public IActionResult Create()
        {
            AddTrainerGroupsToView();
            var trainers = _uow.TrainerRepository.List(new Specification<Trainer>("User"));
            var potentialTrainers =
                _uow.UserRepository.List(new Specification<User>(user =>
                    !trainers.Select(tr => tr.User.Id).Contains(user.Id)));
            ViewBag.PotentialTrainers = potentialTrainers;

            return View();
        }

        [HttpPost("admin/trainers/create")]
        public IActionResult Create(TrainerViewModel trainerView)
        {
            var user = _uow.UserRepository.Find(trainerView.Id);

            if (user == null) ModelState.AddModelError("Id", "User doesn't exist");

            var trainer = _uow.TrainerRepository.Find(trainerView.Id);

            if (trainer != null) ModelState.AddModelError("Id", "User has already been a trainer");

            var group = _uow.TrainerGroupRepository.Find(trainerView.TrainerGroupId);

            if (group == null) ModelState.AddModelError("TrainerGroupId", "Trainer group doesn't exist");

            if (!ModelState.IsValid)
            {
                AddTrainerGroupsToView();
                var trainers = _uow.TrainerRepository.List(new Specification<Trainer>("User"));
                var potentialTrainers =
                    _uow.UserRepository.List(new Specification<User>(user =>
                        !trainers.Select(tr => tr.User.Id).Contains(user.Id)));
                ViewBag.PotentialTrainers = potentialTrainers;

                return View();
            }

            var newTrainer = trainerView.Convert();

            if (user != null && group != null) user.Department = group.Name;

            _uow.TrainerRepository.Add(newTrainer);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [HttpGet("admin/trainers/{trainerId:guid}/edit")]
        public IActionResult Edit(Guid trainerId)
        {
            var trainer =
                _uow.TrainerRepository.Find(new Specification<Trainer>(trainer => trainer.Id.Equals(trainerId), "User",
                    "TrainerGroup"));

            if (trainer == null) return NotFound();

            AddTrainerGroupsToView();

            return View(new TrainerViewModel(trainer));
        }

        [HttpPost("admin/trainers/{trainerId:guid}/edit")]
        public IActionResult Edit(TrainerViewModel trainerView)
        {
            var user = _uow.UserRepository.Find(trainerView.Id);

            if (user == null) ModelState.AddModelError("Id", "User doesn't exist");

            var group = _uow.TrainerGroupRepository.Find(trainerView.TrainerGroupId);

            if (group == null) ModelState.AddModelError("TrainerGroupId", "Trainer group doesn't exist");

            if (!ModelState.IsValid)
            {
                AddTrainerGroupsToView();

                return View(trainerView);
            }

            var trainer = _uow.TrainerRepository.Find(trainerView.Id);

            if (trainer == null) return NotFound();

            trainerView.UpdateEntity(trainer);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [HttpGet("admin/trainers/{trainerId:guid}/delete")]
        public IActionResult Delete(Guid trainerId)
        {
            var trainer =
                _uow.TrainerRepository.Find(new Specification<Trainer>(tr => tr.Id.Equals(trainerId), "User",
                    "TrainerGroup"));

            if (trainer == null) return NotFound();

            return PartialView(new TrainerViewModel(trainer));
        }

        [Route("admin/trainers/{id:guid}/delete/confirm")]
        public IActionResult DeleteConfirm(Guid id)
        {
            var trainer =
                _uow.TrainerRepository.Find(new Specification<Trainer>(trainer => trainer.Id.Equals(id), "User",
                    "TrainerGroup"));

            if (trainer == null) return NotFound();

            _uow.TrainerRepository.Remove(trainer);
            _uow.Commit();

            return RedirectToAction("Index");
        }
    }
}
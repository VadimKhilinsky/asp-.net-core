﻿using System;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class CourseGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseGroupController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("admin/courseGroups")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("admin/courseGroups/create/")]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost("admin/courseGroups/create/")]
        public IActionResult Create(CourseGroupViewModel courseGroup)
        {
            var group = _uow.CourseGroupRepository.Find(
                new Specification<CourseGroup>(cg => cg.Title.Equals(courseGroup.Title, StringComparison.OrdinalIgnoreCase)));

            if (group != null) ModelState.AddModelError("Title", "Course group with such title has already exist");

            if (!ModelState.IsValid) return PartialView();

            var courseEntity = CourseGroup.Create(courseGroup.Title);

            _uow.CourseGroupRepository.Add(courseEntity);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [HttpGet("admin/courseGroups/{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            var group = _uow.CourseGroupRepository.Find(id);

            return PartialView(new CourseGroupViewModel(group));
        }

        [HttpPost("admin/courseGroups/{id:guid}/edit")]
        public IActionResult Edit(CourseGroupViewModel model)
        {
            var group = _uow.CourseGroupRepository.Find(
                new Specification<CourseGroup>(x => x.Title.Equals(model.Title, StringComparison.OrdinalIgnoreCase)));

            if (group != null) ModelState.AddModelError("Title", "This group already exists.");

            if (!ModelState.IsValid) return PartialView();

            var old = _uow.CourseGroupRepository.Find(model.Id);
            old.Title = model.Title;
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [Route("admin/courseGroups/{id:guid}/delete")]
        public IActionResult Delete(Guid id)
        {
            var group = _uow.CourseGroupRepository.Find(id);

            if (group == null) return NotFound();

            var dependents = _uow.CourseGroupRepository.Dependents(id);
            if (dependents.Count > 0)
            {
                ViewBag.Dependents = dependents;
                return PartialView("DeleteFail", new CourseGroupViewModel(group));
            }

            return PartialView(new CourseGroupViewModel(group));
        }

        [Route("admin/courseGroups/{id:guid}/delete/confirm")]
        public IActionResult DeleteConfirm(Guid id)
        {
            var group = _uow.CourseGroupRepository.Find(id);

            if (group == null) return NotFound();

            _uow.CourseGroupRepository.Remove(group);
            _uow.Commit();

            return RedirectToAction("Index");
        }
    }
}
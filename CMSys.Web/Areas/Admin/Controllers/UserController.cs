﻿using System;
using System.Security.Claims;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class UserController : Controller
    {
        private readonly IUnitOfWork _uow;

        public UserController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("admin/users/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("admin/users/{id:guid}")]
        public IActionResult Information(Guid id)
        {
            var user = _uow.UserRepository.Find(id);

            if (user == null) return NotFound();

            ViewBag.RMSysLink = TrainerViewModel.RMSysLink + user.Id;

            return PartialView(new UserViewModel(user));
        }

        [HttpGet("admin/users/{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            var user = _uow.UserRepository.Find(id);

            if (user == null) return NotFound();

            return View(new UserViewModel(user));
        }

        [HttpPost("admin/users/{id:guid}/edit")]
        public IActionResult Edit(UserViewModel userView)
        {
            var user = _uow.UserRepository.Find(userView.Id);

            if (user == null) ModelState.AddModelError("Id", "User with such id doesn't exist");

            if (!ModelState.IsValid) return View(userView);

            userView.UpdateEntity(user);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [Route("admin/users/{id:guid}/roles")]
        public IActionResult Roles(Guid id)
        {
            var user = _uow.UserRepository.Find(id);

            if (user == null) return NotFound();

            if (HttpContext.User.HasClaim(x => x.Type == ClaimTypes.Email && x.Value.Equals(user.Email)))
                return View("RolesUpdateFail", new UserViewModel(user));

            return View(new UserViewModel(user));
        }
    }
}
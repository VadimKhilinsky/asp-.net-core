﻿using System;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class TrainerGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerGroupController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("admin/trainerGroups/")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("admin/trainerGroups/create/")]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost("admin/trainerGroups/create/")]
        public IActionResult Create(TrainerGroupViewModel model)
        {
            var group = _uow.TrainerGroupRepository.Find(
                new Specification<TrainerGroup>(x => x.Name.Equals(model.Name, StringComparison.OrdinalIgnoreCase)));

            if (group != null) ModelState.AddModelError("Name", "This group already exists.");

            if (!ModelState.IsValid) return PartialView();

            var entity = TrainerGroup.Create(model.Name);

            _uow.TrainerGroupRepository.Add(entity);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [HttpGet("admin/trainerGroups/{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            var group = _uow.TrainerGroupRepository.Find(id);

            if (group == null) return NotFound();

            return PartialView(new TrainerGroupViewModel(group));
        }

        [HttpPost("admin/trainerGroups/{id:guid}/edit")]
        public IActionResult Edit(TrainerGroupViewModel model)
        {
            var group = _uow.TrainerGroupRepository.Find(
                new Specification<TrainerGroup>(x => x.Name.Equals(model.Name, StringComparison.OrdinalIgnoreCase)));

            if (group != null) ModelState.AddModelError("Name", "This group already exists.");

            if (!ModelState.IsValid) return PartialView();

            var old = _uow.TrainerGroupRepository.Find(model.Id);
            old.Name = model.Name;
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [Route("admin/trainerGroups/{id:guid}/delete")]
        public IActionResult Delete(Guid id)
        {
            var group = _uow.TrainerGroupRepository.Find(id);

            if (group == null) return NotFound();

            var dependents = _uow.TrainerGroupRepository.Dependents(id);
            if (dependents.Count > 0)
            {
                ViewBag.Dependents = dependents;
                return PartialView("DeleteFail", new TrainerGroupViewModel(group));
            }

            return PartialView(new TrainerGroupViewModel(group));
        }

        [Route("admin/trainerGroups/{id:guid}/delete/confirm")]
        public IActionResult DeleteConfirm(Guid id)
        {
            var group = _uow.TrainerGroupRepository.Find(id);

            if (group == null) return NotFound();

            _uow.TrainerGroupRepository.Remove(group);
            _uow.Commit();

            return RedirectToAction("Index");
        }
    }
}
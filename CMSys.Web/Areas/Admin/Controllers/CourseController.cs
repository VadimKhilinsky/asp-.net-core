﻿using System;
using System.Linq;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        private void AddCourseGroupsToView()
        {
            ViewBag.groups = _uow.CourseGroupRepository.List().Select(group => new CourseGroupViewModel(group));
        }

        [Route("admin/courses")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("admin/courses/create")]
        public IActionResult Create()
        {
            AddCourseGroupsToView();

            return View();
        }

        [HttpPost("admin/courses/create")]
        public IActionResult Create(CourseViewModel courseView)
        {
            var selectedGroup = _uow.CourseGroupRepository.Find(courseView.CourseGroupId);

            if (selectedGroup == null)
                ModelState.AddModelError("CourseGroupId", "Course Group with such id doesn't exist!");

            if (!ModelState.IsValid)
            {
                AddCourseGroupsToView();

                return View();
            }

            var course = courseView.Convert();

            _uow.CourseRepository.Add(course);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [HttpGet("admin/courses/{id:guid}/edit")]
        public IActionResult Edit(Guid id)
        {
            var selectedCourse = _uow.CourseRepository.Find(id);

            if (selectedCourse == null) return NotFound();

            AddCourseGroupsToView();
            return View(new CourseViewModel(selectedCourse));
        }

        [HttpPost("admin/courses/{id:guid}/edit")]
        public IActionResult Edit(CourseViewModel course)
        {
            var selectedGroup = _uow.CourseGroupRepository.Find(course.CourseGroupId);

            if (selectedGroup == null)
                ModelState.AddModelError("CourseGroupId", "Course Group with such id doesn't exist!");

            if (!ModelState.IsValid)
            {
                AddCourseGroupsToView();
                return View(course);
            }

            var oldCourse = _uow.CourseRepository.Find(course.Id);

            if (oldCourse == null) return NotFound();

            course.UpdateEntity(oldCourse);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [HttpGet("admin/courses/{id:guid}/delete")]
        public IActionResult Delete(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);

            if (course == null) return NotFound();

            var trainers = _uow.CourseTrainerRepository.List(
                new Specification<CourseTrainer>(ct => ct.CourseId.Equals(course.Id), "Course", "Trainer",
                    "Trainer.User"));
            var courseView = new CourseViewModel(course, trainers.Select(ct => ct.Trainer));

            return PartialView(courseView);
        }

        [Route("admin/courses/{id:guid}/delete/confirm")]
        public IActionResult DeleteConfirm(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);

            if (course == null) return NotFound();

            _uow.CourseRepository.Remove(course);
            _uow.Commit();

            return RedirectToAction("Index");
        }

        [Route("admin/courses/{id:guid}/trainers")]
        public IActionResult Trainers(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);

            if (course == null) return NotFound();

            var trainers = _uow.CourseTrainerRepository
                .List(new Specification<CourseTrainer>(ct => ct.CourseId.Equals(id), "Course", "Trainer",
                    "Trainer.User"))
                .Select(x => x.Trainer);

            return View(new CourseViewModel(course, trainers));
        }
    }
}
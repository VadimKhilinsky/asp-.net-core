﻿using System;
using System.Linq;
using CMSys.Common.Paging;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Entities.Filters;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Api.Controllers
{
    [ApiController]
    [Area("Api")]
    public class CourseController : ControllerBase
    {
        private readonly IUnitOfWork _uow;

        public CourseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Authorize("User")]
        [HttpGet("api/courses")]
        public ActionResult<CourseViewModel> Get([FromQuery] Arrangement? arrangement,
            [FromQuery] CourseType? courseType, bool isNew, int? page, int? perPage)
        {
            var courseTrainersPairs =
                _uow.CourseTrainerRepository.List(new Specification<CourseTrainer>("Course", "Trainer",
                    "Trainer.User"));

            var courses = _uow.CourseRepository.GetPagedList(
                new FilteredPageInfo(new CourseFilter(arrangement, courseType, isNew), page ?? 1, perPage ?? 10),
                new Specification<Course>("CourseGroup")
            ).Convert(course => new CourseViewModel(
                course,
                courseTrainersPairs.Where(courseTrainer => courseTrainer.CourseId == course.Id)
                    .Select(ct => ct.Trainer))
            );

            return new ObjectResult(courses);
        }

        [Authorize("Admin")]
        [HttpGet("api/courses/{id:guid}/trainers")]
        public IActionResult GetTrainersAndPotentialTrainers(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);

            if (course == null) return NotFound();

            var trainers = _uow.CourseTrainerRepository
                .List(new Specification<CourseTrainer>(ct => ct.CourseId.Equals(id), "Trainer", "Trainer.User"))
                .Select(ct => ct.Trainer).OrderBy(tr => tr.User.FirstName);
            var potentialTrainer = _uow.TrainerRepository
                .List(new Specification<Trainer>(tr => !trainers.Contains(tr), "User"))
                .OrderBy(tr => tr.User.FirstName);

            var trainerViews = trainers.Select(tr => new TrainerViewModel(tr));
            var potentialTrainerViews = potentialTrainer.Select(tr => new TrainerViewModel(tr));

            return new ObjectResult(new {trainerViews, potentialTrainerViews});
        }

        [Authorize("Admin")]
        [HttpPut("api/courses/{courseId:guid}/trainers/add/{trainerId:guid}")]
        public IActionResult AddTrainer(Guid courseId, Guid trainerId)
        {
            var course = _uow.CourseRepository.Find(courseId);

            if (course == null) return NotFound();

            var trainer = _uow.TrainerRepository.Find(trainerId);

            if (trainer == null) return NotFound();

            var courseTrainer = _uow.CourseTrainerRepository.Find(courseId, trainerId);

            if (courseTrainer != null) return BadRequest();

            var newCourseTrainer = new CourseTrainer {CourseId = courseId, TrainerId = trainerId};

            _uow.CourseTrainerRepository.Add(newCourseTrainer);
            _uow.Commit();

            return Ok();
        }

        [Authorize("Admin")]
        [HttpDelete("api/courses/{courseId:guid}/trainers/remove/{trainerId:guid}")]
        public IActionResult RemoveTrainer(Guid courseId, Guid trainerId)
        {
            var courseTrainer = _uow.CourseTrainerRepository.Find(courseId, trainerId);

            if (courseTrainer == null) return NotFound();

            _uow.CourseTrainerRepository.Remove(courseTrainer);
            _uow.Commit();

            return Ok();
        }
    }
}
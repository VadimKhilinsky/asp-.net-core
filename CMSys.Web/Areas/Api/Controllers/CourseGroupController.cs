﻿using System.Linq;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Api.Controllers
{
    [ApiController]
    [Area("Api")]
    [Authorize("Admin")]
    public class CourseGroupController : ControllerBase
    {
        private readonly IUnitOfWork _uow;

        public CourseGroupController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [HttpGet("api/courseGroups")]
        public IActionResult Get()
        {
            var groups = _uow.CourseGroupRepository.List();

            return new ObjectResult(groups.Select(group => new CourseGroupViewModel(group)));
        }
    }
}
﻿using System.Linq;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Api.Controllers
{
    [ApiController]
    [Authorize("User")]
    [Area("Api")]
    public class TrainerController : ControllerBase
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [HttpGet("api/trainers")]
        public ActionResult<TrainerViewModel> Get()
        {
            var trainers = _uow.TrainerRepository.List(new Specification<Trainer>("User", "TrainerGroup"));
            var trainerViews = trainers.Select(trainer => new TrainerViewModel(trainer));

            return new ObjectResult(trainerViews);
        }


        [HttpGet("api/groups")]
        public ActionResult<TrainerGroupViewModel> GetGroups()
        {
            var trainers = _uow.TrainerRepository.List(new Specification<Trainer>("User", "TrainerGroup"));
            var trainerViews = trainers.Select(trainer => new TrainerViewModel(trainer));
            var trainerGroups = _uow.TrainerGroupRepository.List(new Specification<TrainerGroup>());
            var trainerGroupViews = trainerGroups.Select(trainerGroup => new TrainerGroupViewModel(trainerGroup,
                trainerViews.Where(trainer => trainer.TrainerGroupId.Equals(trainerGroup.Id))));

            return new ObjectResult(trainerGroupViews);
        }
    }
}
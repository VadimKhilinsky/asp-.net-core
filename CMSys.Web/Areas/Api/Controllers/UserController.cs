﻿using System;
using System.Linq;
using CMSys.Common.Paging;
using CMSys.Core.Entities.Filters;
using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Api.Controllers
{
    [Area("Api")]
    [Authorize("Admin")]
    public class UserController : Controller
    {
        private readonly IUnitOfWork _uow;

        public UserController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("api/users/")]
        public IActionResult Get(string title, int? page, int? perPage)
        {
            var users = _uow.UserRepository.GetPagedList(new FilteredPageInfo(new UserFilter(title), page ?? 1,
                perPage ?? 10));
            var list = users.Convert(user => new UserViewModel(user));

            return new ObjectResult(list);
        }

        [Route("api/users/{id:guid}/roles/")]
        public IActionResult Roles(Guid id)
        {
            var user = _uow.UserRepository.Find(new Specification<User>(x => x.Id.Equals(id), "Roles.Role"));

            if (user == null) return NotFound();

            var roles = user.Roles.Select(x => x.Role);
            var potentialRoles = _uow.RoleRepository.List(new Specification<Role>(x => !roles.Contains(x)));

            return new ObjectResult(new {roles, potentialRoles});
        }

        [HttpPut("api/users/{userId:guid}/roles/add/{roleId:int}")]
        public IActionResult AddRole(Guid userId, int roleId)
        {
            var user = _uow.UserRepository.Find(new Specification<User>(x => x.Id.Equals(userId), "Roles.Role"));

            if (user == null) return NotFound();

            var role = _uow.RoleRepository.Find(roleId);

            if (role == null) return NotFound();

            if (user.Roles.Any(x => x.RoleId.Equals(roleId))) return BadRequest();

            var userRole = new UserRole {UserId = userId, RoleId = roleId};
            user.Roles.Add(userRole);
            _uow.Commit();

            return Ok();
        }

        [HttpDelete("api/users/{userId:guid}/roles/delete/{roleId:int}")]
        public IActionResult RemoveRole(Guid userId, int roleId)
        {
            var user = _uow.UserRepository.Find(new Specification<User>(x => x.Id.Equals(userId), "Roles.Role"));

            if (user == null) return NotFound();

            var role = _uow.RoleRepository.Find(roleId);

            if (role == null) return NotFound();

            var userRole = user.Roles.FirstOrDefault(x => x.RoleId.Equals(roleId) && x.UserId.Equals(userId));

            if (userRole == null) return BadRequest();

            user.Roles.Remove(userRole);
            _uow.Commit();

            return Ok();
        }
    }
}
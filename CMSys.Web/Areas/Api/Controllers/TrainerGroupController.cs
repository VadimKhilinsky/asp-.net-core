﻿using System.Linq;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Api.Controllers
{
    [ApiController]
    [Area("Api")]
    [Authorize("Admin")]
    public class TrainerGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerGroupController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [HttpGet("api/trainerGroups/", Order = 1)]
        public IActionResult TrainerGroups()
        {
            var groups = _uow.TrainerGroupRepository.List().Select(x => new TrainerGroupViewModel(x));
            return new ObjectResult(groups);
        }
    }
}
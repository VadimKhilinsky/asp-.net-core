﻿var page = 1;
var perPage = 6;
var totalPages = 1;
var total = 0;

$(document).ready(function () {
    getUsers("/api/users");
});

$(document).on("change", "#form input", function () {
    page = 1;
    getUsers("/api/users");
});

$(document).on("change", "#itemsPerPage", onItemsPerPageChanged);

$(document).on("click", "#pager button", function (e) {
    e.preventDefault();
    switch ($(this).attr("data-page")) {
        case "prev": page--; break;
        case "next": page++; break;
        case "first": page = 1; break;
        case "last": page = totalPages; break;
    }
    var url = "/api/users";
    getUsers(url, page);
});

function onPageChanged(data) {
    page <= 1 ? $("#pager button.prev").attr("disabled", true) : $("#pager button.prev").attr("disabled", false);
    page >= totalPages ? $("#pager button.next").attr("disabled", true) : $("#pager button.next").attr("disabled", false);
    $("#page").text(page);
    $("#page-info").text(`${(page - 1) * perPage + 1}-${page * perPage > data.total ? data.total : page * perPage} of ${data.total} items`);
}

function getUsers(url, page) {
    var query = $("#form").serialize();
    page = (page == undefined) ? 1 : page;
    fetch(url + '?' + query + "&page=" + page + '&perPage=' + perPage,
            {
                method: "GET"
            })
        .then(data => data.json()).then(data => onUsersGet(data));
}

function onUsersGet(data) {
    var html = '';
    totalPages = data.totalPages;
    total = data.total;

    for (var user of data.items) {
        html += `<tr><td><a class="item"  href="/admin/users/${user.id}">${user.fullName}</a></td>
                        <td><img src="${user.photo}"></td>
                        <td>${user.department}</td>
                        <td>${user.position || ''}</td>
                        <td>${user.location || ''}</td>
                        <td>${user.active ? 'Yes' : 'No'}</td>
                        <td>
                        <div>
                        <a class="btn btn-light btn-primitive-hover btn-icon font-weight-bold border" href="/admin/users/${user.id}/edit">
                        <i class="fa fa-edit"></i></a>
                        <a class="btn btn-light btn-primitive-hover btn-icon font-weight-bold border" href="/admin/users/${user.id}/roles">
                        <i class="fa fa-user-plus"></i></a>
                        <div>
                        </td></tr>`;
    }
    $("#data").html(html);
    onPageChanged(data);
}

function onItemsPerPageChanged() {
    page = 1;
    var count = $("#itemsPerPage").val();
    perPage = count === "All" ? total : parseInt(count);
    getUsers("/api/users");
}
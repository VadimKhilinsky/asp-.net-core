﻿var id = $("#modelId").val();

$(document).ready(function () {
    getRoles();
});

function getRoles() {
    fetch(`/api/users/${id}/roles`,
            {
                method: "GET"
            })
        .then(data => data.json())
        .then(data => {
            onRolesGet(data.roles);
            onPotentialRolesGet(data.potentialRoles);
        });
}

function onRolesGet(data) {
    var html = '';
    for (var item of data) {
        html += `<tr><td>${item.name}</td>
                    <td class="text-center"><a class="btn btn-light btn-danger-hover border remove" data-id="${item.id
            }" href="api/users/${id}/roles/delete/${item.id}">Remove</a></td></tr>`;
    }
    $("#data").html(html);
}

function onPotentialRolesGet(data) {
    var selector = $("#potentialRoles").get(0);
    selector.options.length = 1;
    for (var item of data) {
        selector.add(new Option(item.name, item.id));
    }
}

$(document).on("change", "#potentialRoles", function () {
    var value = $("#potentialRoles").get(0).value;
    if (!value) {
        $("#add").attr("disabled", true);
    } else {
        $("#add").attr("disabled", false);
    }
});

$(document).on("click", "a.remove", function (e) {
    e.preventDefault();
    fetch(`/api/users/${id}/roles/delete/${$(this).attr("data-id")}`,
            {
                method: "DELETE"
            })
        .then(() => getRoles());
});

$(document).on("click", "#add", function (e) {
    e.preventDefault();
    var roleId = $("#potentialRoles").val();
    fetch(`/api/users/${id}/roles/add/${roleId}`,
            {
                method: "PUT"
            })
        .then(() => getRoles());
});
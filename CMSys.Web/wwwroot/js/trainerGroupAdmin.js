﻿$(document).ready(getTrainerGroups);

function getTrainerGroups() {
    fetch("/api/trainerGroups/",
        {
            method: "GET"
        })
        .then(data => data.json())
        .then(data => onTrainerGroupsGet(data));
}

function onTrainerGroupsGet(data) {
    var html = '';
    for (var item of data) {
        html += `<tr class="row"><td class="col-10">${item.name}</td><td class="col-2 px-0"><div>
                             <a class="btn btn-light btn-primitive-hover btn-icon font-weight-bold border item" href="/admin/trainerGroups/${item.id}/edit/"><i class="fa fa-edit"></i></a>
                             <a class="btn btn-light btn-danger-hover btn-icon font-weight-bold border item" href="/admin/trainerGroups/${item.id}/delete/"><i class="fa fa-trash"></i></a>
                             </div></td></tr>`;
    }
    $("#trainerGroupData").html(html);
} 
﻿function getCourses(data, courseType, arrangement) {
    var html = '';
    totalPages = data.totalPages;
    total = data.total;
    for (var item of data.items) {
        html += `<tr><td class="font-weight-bold"><a title="#courseLink" class="item" href="/courses/${item.id}">${item.title}${item.isNew
            ? `<span class="new" title="New course!!!">&#11088;</span>`
            : ''}</a></td>
            <td>${courseType[item.courseType]}</td>
            <td>${arrangement[item.arrangement]}</td>`;
        html += `<td class="py-1">`;
        for (var trainer of item.trainers) {
            html += `<a class="badge badge-primitive my-0 item" href="/trainers/${trainer.id}">${trainer.firstName} ${trainer.lastName}</a><br>`
        }
        html += `</td><td class="py-auto">
                <a class="btn btn-light btn-primitive-hover font-weight-bold border btn-icon" href="/admin/courses/${item.id
            }/edit">
                    <i class="fa fa-edit"></i></a>
                <a class="btn btn-light btn-primitive-hover font-weight-bold border btn-icon" href="/admin/courses/${item.id
            }/trainers">
                    <i class="fa fa-user"></i></a>
                <a class="btn btn-light btn-danger-hover font-weight-bold border btn-icon item" href="/admin/courses/${
            item.id}/delete">
                    <i class="fa fa-trash"></i></a></td></tr>`;
        html += `<td>`;
    }
    $("#coursesData").html(html);
    onPageChanged(data);
}
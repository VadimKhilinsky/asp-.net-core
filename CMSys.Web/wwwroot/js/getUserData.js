﻿function getCourses(data, courseType, arrangement) {
    var html = '';
    totalPages = data.totalPages;
    total = data.total;
    for (var item of data.items) {
        html += `<tr><td class="font-weight-bold"><a title="#courseLink" class="item" href="/courses/${item.id}">${item.title}${item.isNew
            ? `<span class="new" title="New course!!!">&#11088;</span>`
            : ''}</a></td>
            <td>${courseType[item.courseType]}</td>
            <td>${arrangement[item.arrangement]}</td>`;
        html += `<td class="py-1">`;
        for (var trainer of item.trainers) {
            html += `<a class="badge badge-primitive my-0 item" href="/trainers/${trainer.id}">${trainer.firstName} ${trainer.lastName}</a><br>`
        }
        html += `<td>`;
    }
    $("#coursesData").html(html);
    onPageChanged(data);
}
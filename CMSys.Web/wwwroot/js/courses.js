﻿var page = 1;
var perPage = 10;
var total = 0;
var totalPages = 1;
var ctJsons = $("#types").val();
var arJsons = $("#arrangements").val();
var courseType = JSON.parse(`${ctJsons}`);
var arrangement = JSON.parse(`${arJsons}`);

$(document).ready(function () {
    loadCourses("/api/courses");
});

$(document).on("change", "#formSelects select, input", function () {
    page = 1;
    loadCourses("/api/courses");
});

function loadCourses(url, page) {
    var query = $("#formSelects").serialize();
    page = (page == undefined) ? 1 : page;
    fetch(url + '?' + query + "&page=" + page + '&perPage=' + perPage,
        {
            method: "GET"
        })
        .then(data => data.json()).then(data => getCourses(data, courseType, arrangement));
}

$(document).on("change", "#itemsPerPage", onItemsPerPageChanged);

$(document).on("click",
    "#pager button",
    function (e) {
        e.preventDefault();
        switch ($(this).attr("data-page")) {
            case "prev":
                page--;
                break;
            case "next":
                page++;
                break;
            case "first":
                page = 1;
                break;
            case "last":
                page = totalPages;
                break;
        }
        loadCourses("/api/courses", page);
    });

function onPageChanged(data) {
    page <= 1 ? $("#pager button.prev").attr("disabled", true) : $("#pager button.prev").attr("disabled", false);
    page >= totalPages ? $("#pager button.next").attr("disabled", true) : $("#pager button.next").attr("disabled", false);
    $("#page").text(page);
    $("#page-info").text(`${(page - 1) * perPage + 1}-${page * perPage > data.total ? data.total : page * perPage} of ${data.total} items`);
}

function onItemsPerPageChanged() {
    page = 1;
    var count = $("#itemsPerPage").val();
    perPage = count === "All" ? total : parseInt(count);
    loadCourses("/api/courses");
}
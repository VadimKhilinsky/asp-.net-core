﻿$(document).on("click", ".item", showModal);

$.ajaxSetup({ cache: false });

function showModal(e) {
    e.preventDefault();
    
    $.get(this.href, function (data) {
        $('#dialogContent').html(data);
        $('#modDialog').modal('show');
    });
}
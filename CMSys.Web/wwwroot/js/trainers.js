﻿$(document).ready(function () {
    loadCourses("/api/groups");
});

function loadCourses(url) {
    fetch(url,
        {
            method: "GET"
        })
        .then(data => data.json()).then(data => getTrainers(data));
}

async function getTrainers(data) {
    var html = ''
    for (var group of data) {
        for (var item of group.trainers) {
            html += `<tr><td>${group.name}</td>
                <td><img src="${item.photo}" class="img-small" /></td>
                <td>${item.firstName} ${item.lastName}</td>
                <td>${item.info === null ? '' : item.info}</td>
                <td><div><a class="btn btn-light btn-primitive-hover btn-icon font-weight-bold border" href="/admin/trainers/${item.id}/edit/"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-light btn-danger-hover btn-icon font-weight-bold border item" href="/admin/trainers/${item.id}/delete/"><i class="fa fa-trash"></i></a>
                    </div></td></tr>`;
        }
    }

    $("#trainersData").html(html);
}
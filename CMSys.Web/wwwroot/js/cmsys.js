﻿$(document).on("click", ".menu-btn", openMenu);
$(document).on("click", onCloseMenu);
var menu = document.getElementById("menu");
var isOpenedMenu = false;

function openMenu() {
    menu.classList.toggle("menu-active");
    let div = document.createElement("div");
    div.id = "fade";
    div.className = "position-fixed cover";
    document.body.appendChild(div);
    isOpenedMenu = true;
}

function menuClick() {
    closeMenu();
}

function onCloseMenu() {
    if (isOpenedMenu !== true) {
        return;
    }

    if (event.pageX >= menu.offsetWidth) {
        closeMenu();
    }
}

function closeMenu() {
    menu.classList.remove("menu-active");
    $("#fade").remove();
}


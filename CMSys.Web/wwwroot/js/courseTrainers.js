﻿var id = $("#modelId").val();

$(document).ready(function () {
    getTrainers();
});

function getTrainers() {
    fetch(`/api/courses/${id}/trainers`,
        {
            method: "GET"
        })
        .then(data => data.json())
        .then(data => {
            onTrainersGet(data.trainerViews);
            onPotentialTrainersGet(data.potentialTrainerViews);
        });
}

function onTrainersGet(data) {
    var html = '';
    for (var item of data) {
        html += `<tr><td>${item.firstName} ${item.lastName}</td>
                    <td class="text-center"><a class="btn btn-light border remove" data-id="${item.id}" href="api/courses/${id}/trainers/remove/${item.id}">Remove</a></td></tr>`;
    }
    $("#courseTrainers").html(html);
}

function onPotentialTrainersGet(data) {
    var selector = $("#coursePotentialTrainers").get(0);
    selector.options.length = 1;
    for (var item of data) {
        selector.add(new Option(item.firstName + " " + item.lastName, item.id));
    }
}

$(document).on("change", "#coursePotentialTrainers", function () {
    var value = $("#coursePotentialTrainers").get(0).value;
    if (!value) {
        $("#add").attr("disabled", true);
    } else {
        $("#add").attr("disabled", false);
    }
});

$(document).on("click", "a.remove", function (e) {
    e.preventDefault();
    fetch(`/api/courses/${id}/trainers/remove/${$(this).attr("data-id")}`,
        {
            method: "DELETE"
        })
        .then(() => getTrainers());
});

$(document).on("click", "#add", function (e) {
    e.preventDefault();
    var trainerId = $("#coursePotentialTrainers").val();
    fetch(`/api/courses/${id}/trainers/add/${trainerId}`,
        {
            method: "PUT"
        })
        .then(() => getTrainers());
});
﻿var trainersDataField = document.getElementById("trainerGroups");

$(document).ready(load);

function load() {
    fetch('api/groups',
        {
            method: "GET"
        })
        .then(data => data.json())
        .then(data => loadGroups(data));
}

function loadGroups(data) {
    var html = ""
    for (var group of data) {
        if (group.trainers.length == 0)
            continue;

        html += `<div><h3>${group.name}</h3></div>`
        html += `<div class="form-inline mx-3">`
        for (var trainer of group.trainers) {
            html += `<div class="card text-black bg-light mx-3" style="max-width: 18rem;">`
            html += `<div class="card-header">${trainer.firstName + " " + trainer.lastName}</div>`
            html += `<img class="card-img-top rounded float-left img-thumbnail" src="${trainer.photo}"/>`;
            html += `<div class="card-body">`
            html += `<a class="btn btn-light item" href="/trainers/${trainer.id}">Additional info</a>`;
            html += `</div>`
            html += `</div>`
        }
        html += `</div>`
    }
    trainersDataField.innerHTML = html;
}
﻿using System.ComponentModel.DataAnnotations;
using CMSys.Core.Entities.Membership;

namespace CMSys.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(User.EmailLength)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
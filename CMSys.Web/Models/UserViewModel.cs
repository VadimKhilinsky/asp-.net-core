﻿using System;
using System.ComponentModel.DataAnnotations;
using CMSys.Core.Entities.Membership;

namespace CMSys.Web.Models
{
    public class UserViewModel
    {
        public UserViewModel()
        {
        }

        public UserViewModel(User user)
        {
            Id = user.Id;
            Email = user.Email;
            FirstName = user.FirstName;
            LastName = user.LastName;
            StartDate = user.StartDate;
            EndDate = user.EndDate;
            Department = user.Department;
            Position = user.Position;
            Location = user.Location;
            Photo = $"data:image/;base64,{Convert.ToBase64String(user.Photo)}";
        }

        public Guid Id { get; set; }

        [StringLength(User.EmailLength)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(User.FirstNameLength)] public string FirstName { get; set; }

        [StringLength(User.FirstNameLength)] public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [DataType(DataType.DateTime)] public DateTime StartDate { get; set; }

        [DataType(DataType.DateTime)] public DateTime? EndDate { get; set; }

        [StringLength(User.DepartmentLength)] public string Department { get; set; }

        [StringLength(User.PositionLength)] public string Position { get; set; }

        [StringLength(User.LocationLength)] public string Location { get; set; }

        public string Photo { get; set; }
        public bool Active => EndDate == null;

        public void UpdateEntity(User user)
        {
            user.Department = Department;
            user.EndDate = EndDate;
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.Location = Location;
            user.StartDate = StartDate;
            user.Position = Position;
        }
    }
}
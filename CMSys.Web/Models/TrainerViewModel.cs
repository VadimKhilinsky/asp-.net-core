﻿using CMSys.Core.Entities.Catalog;
using System;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Web.Models
{
    public class TrainerViewModel
    {
        public const string RMSysLink = "https://rmsys.issoft.by/Individual/Index#id-";

        public Guid Id { get; set; }
        [StringLength(Trainer.InfoLength)]
        public string Info { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid TrainerGroupId { get; set; }
        public string Photo { get; set; }
        public string Department { get; set; }

        public string TrainerLink => RMSysLink + Id;

        public TrainerViewModel()
        {

        }

        public TrainerViewModel(Trainer trainer)
        {
            Id = trainer.Id;
            Department = trainer.User.Department;
            FirstName = trainer.User.FirstName;
            Info = trainer.Info;
            LastName = trainer.User.LastName;
            Photo = $"data:image/;base64,{System.Convert.ToBase64String(trainer.User.Photo)}";
            TrainerGroupId = trainer.TrainerGroupId;
        }

        public Trainer Convert()
        {
            return new Trainer
            {
                Id = this.Id,
                Info = this.Info,
                TrainerGroupId = this.TrainerGroupId,
            };
        }

        public void UpdateEntity(Trainer trainer)
        {
            trainer.Info = Info;
            trainer.TrainerGroupId = TrainerGroupId;
        }
    }
}

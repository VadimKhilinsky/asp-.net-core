﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.Models
{
    public class CourseViewModel
    {
        public CourseViewModel()
        {
        }

        public CourseViewModel(Course course, IEnumerable<Trainer> trainers = null)
        {
            Id = course.Id;
            Title = course.Title;
            Info = course.Info;
            IsNew = course.IsNew;
            CourseType = course.CourseType;
            Arrangement = course.Arrangement;
            CourseGroupId = course.GroupId;
            if (trainers != null)
                Trainers = new List<TrainerViewModel>(trainers.Select(trainer => new TrainerViewModel(trainer)));
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(Course.TitleLength, MinimumLength = 1)]
        public string Title { get; set; }

        [StringLength(Course.InfoLength)] public string Info { get; set; }

        [Required] public bool IsNew { get; set; }

        [Required] public CourseType CourseType { get; set; }

        [Required] public Arrangement Arrangement { get; set; }

        public Guid CourseGroupId { get; set; }
        public IEnumerable<TrainerViewModel> Trainers { get; set; }

        public Course Convert()
        {
            return new Course
            {
                Id = Id,
                Title = Title,
                Info = Info,
                IsNew = IsNew,
                CourseType = CourseType,
                Arrangement = Arrangement,
                GroupId = CourseGroupId
            };
        }

        public void UpdateEntity(Course course)
        {
            course.Title = Title;
            course.IsNew = IsNew;
            course.Info = Info;
            course.CourseType = CourseType;
            course.Arrangement = Arrangement;
            course.GroupId = CourseGroupId;
        }
    }
}
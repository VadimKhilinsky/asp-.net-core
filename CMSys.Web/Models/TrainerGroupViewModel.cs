﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.Models
{
    public class TrainerGroupViewModel
    {
        public TrainerGroupViewModel()
        {
        }

        public TrainerGroupViewModel(TrainerGroup trainerGroup, IEnumerable<TrainerViewModel> trainers = null)
        {
            Id = trainerGroup.Id;
            Name = trainerGroup.Name;
            Trainers = trainers;
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(TrainerGroup.NameLength)]
        public string Name { get; set; }

        public IEnumerable<TrainerViewModel> Trainers { get; set; }
    }
}
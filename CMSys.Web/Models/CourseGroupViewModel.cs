﻿using System;
using System.ComponentModel.DataAnnotations;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.Models
{
    public class CourseGroupViewModel
    {
        public CourseGroupViewModel()
        {
        }

        public CourseGroupViewModel(CourseGroup group)
        {
            Id = group.Id;
            Title = group.Title;
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(CourseGroup.TitleLength, MinimumLength = 1)]
        public string Title { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace CMSys.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUnitOfWork _uow;

        public AccountController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [HttpGet("account/login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("account/login")]
        public IActionResult Login(LoginViewModel loginView)
        {
            var identity = GetIdentity(loginView.Email, loginView.Password);
            if (identity == null || !ModelState.IsValid) return View();
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                AuthOptions.ISSUER,
                AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromHours(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            HttpContext.Session.SetString("JWToken", encodedJwt);

            return RedirectToAction("Index", "Home");
        }

        private ClaimsIdentity GetIdentity(string email, string password)
        {
            var user = _uow.UserRepository.Find(new Specification<User>(usr => usr.Email.Equals(email),
                "Roles.Role"));

            if (user == null)
            {
                ModelState.AddModelError("Email", "Invalid email");
                return null;
            }

            if (!user.VerifyPassword(password))
            {
                ModelState.AddModelError("Password", "Invalid password");
                return null;
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.FirstName),
                new Claim(ClaimTypes.Email, user.Email)
            };
            var roles = user.Roles.Select(role => new Claim(ClaimTypes.Role, role.Role.Name)).ToList();
            claims.AddRange(roles);
            var id = new ClaimsIdentity(claims, "Token");
            return id;
        }

        [Route("account/logout")]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
﻿using System;
using System.Linq;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize("User")]
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("courses/")]
        public IActionResult Index()
        {
            ViewBag.Type = Enum.GetNames(typeof(CourseType));
            ViewBag.Arrangement = Enum.GetNames(typeof(Arrangement));
            return View();
        }

        [Route("courses/{id:guid}")]
        public IActionResult Information(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);

            if (course == null) return NotFound();

            var trainers = _uow.CourseTrainerRepository
                .List(new Specification<CourseTrainer>(x => x.CourseId.Equals(id), "Course", "Trainer", "Trainer.User"))
                .Select(x => x.Trainer);

            var model = new CourseViewModel(course, trainers);

            return PartialView(model);
        }
    }
}
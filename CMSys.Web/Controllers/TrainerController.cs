﻿using System;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize("User")]
    public class TrainerController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("/trainers/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/trainers/{id:guid}")]
        public IActionResult Information(Guid id)
        {
            var trainer = _uow.TrainerRepository.Find(
                new Specification<Trainer>(tr => tr.Id.Equals(id), "User", "TrainerGroup"));

            if (trainer == null) return NotFound();

            return PartialView(new TrainerViewModel(trainer));
        }
    }
}
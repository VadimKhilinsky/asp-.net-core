﻿using System;
using System.Linq;
using System.Text;
using CMSys.Common.Paging;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories;
using CMSys.Data;

namespace CMSys.Tester
{
    public class Program
    {
        public static void Main()
        {
            // repositories tests

            // setup
            var options = new UnitOfWorkOptions
            {
                ConnectionString = "data source=(local);Initial Catalog=CMSys;Integrated Security=True;",
                CommandTimeout = 30
            };

            Console.OutputEncoding = Encoding.UTF8;

            using IUnitOfWork uow = new UnitOfWork(options);

            // roles
            var roleRepository = uow.RoleRepository;
            var role = roleRepository.Find(1);
            Console.WriteLine(role.Name);
            Console.ReadLine();

            var roles = roleRepository.List();
            foreach (var element in roles)
            {
                Console.WriteLine(element.Name);
            }

            Console.ReadLine();

            // users
            var userRepository = uow.UserRepository;
            foreach (var element in userRepository.List())
            {
                Console.WriteLine(element.Email);
            }

            Console.ReadLine();

            foreach (var element in userRepository.GetPagedList(new FilteredPageInfo("vol")).Items)
            {
                Console.WriteLine(element.Email);
            }

            Console.ReadLine();

            foreach (var element in userRepository
                .GetPagedList(new FilteredPageInfo("vol"), new Specification<User>("Roles.Role")).Items)
            {
                Console.WriteLine(element.Email);
            }

            Console.ReadLine();

            // trainers

            var trainerRepository = uow.TrainerRepository;
            ISpecification<Trainer> spec = new Specification<Trainer>("User", "TrainerGroup", "User.Roles.Role");
            foreach (var item in trainerRepository.List(spec))
            {
                Console.WriteLine(item.User.FullName);
                foreach (var name in item.User.Roles.Select(x => x.Role.Name))
                {
                    Console.WriteLine(name);
                }
                Console.WriteLine(item.TrainerGroup.Name);
            }

            Console.ReadLine();

            var def = new string('-', 10);
            Encoding win1252 = Encoding.GetEncoding("windows-1252");

            var courseRepository = uow.CourseRepository;
            ISpecification<Course> specc = new Specification<Course>("CourseGroup");
            foreach(var item in courseRepository.List(specc))
            {
                Console.WriteLine(item.Title);
                Console.WriteLine(item.Info);
                Console.WriteLine(item.CourseGroup.Title);
                Console.WriteLine(item.Arrangement);
                Console.WriteLine(item.CourseType);
                Console.WriteLine(def);
            }

            Console.ReadLine();




        }
    }
}
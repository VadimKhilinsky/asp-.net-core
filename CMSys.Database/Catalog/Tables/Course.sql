﻿CREATE TABLE [Catalog].[Course]
(
    [Id] UNIQUEIDENTIFIER CONSTRAINT [PK_Catalog_Course] PRIMARY KEY,
    [Title] NVARCHAR(64) NOT NULL,
    [Info] NVARCHAR(4000) NULL,
    [IsNew] BIT NOT NULL,
    [CourseType] INT NOT NULL CONSTRAINT [CC_Catalog_Course_CourseType] CHECK (CourseType IN (0, 1)),
    [Arrangement] INT NOT NULL CONSTRAINT [CC_Catalog_Course_Arrangment] CHECK(Arrangement IN (0, 1)),
    [GroupId] UNIQUEIDENTIFIER NOT NULL
        CONSTRAINT [FK_Catalog_Course_Catalog_CourseGroup] FOREIGN KEY
        REFERENCES [Catalog].[CourseGroup] ([Id]),
    [Version] ROWVERSION NOT NULL
);
GO

CREATE INDEX [IX_Catalog_Course_CourseGroupId] ON [Catalog].[Course]([GroupId]);
GO

﻿CREATE TABLE [Catalog].[CourseGroup]
(
    [Id] UNIQUEIDENTIFIER CONSTRAINT [PK_Catalog_CourseGroup] PRIMARY KEY,
    [Title] NVARCHAR(64) NOT NULL CONSTRAINT [UK_Catalog_CourseGroup_Title] UNIQUE,
    [Version] ROWVERSION NOT NULL
)

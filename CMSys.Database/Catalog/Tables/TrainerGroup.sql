﻿CREATE TABLE [Catalog].[TrainerGroup]
(
    [Id] UNIQUEIDENTIFIER CONSTRAINT [PK_Catalog_TrainerGroup] PRIMARY KEY,
    [Name] NVARCHAR(32) NOT NULL CONSTRAINT [UK_Catalog_TrainerGroup_Name] UNIQUE,
    [Version] ROWVERSION NOT NULL
)

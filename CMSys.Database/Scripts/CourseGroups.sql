﻿INSERT INTO [Catalog].[CourseGroup]([Id], [Title])
VALUES
    ('8869be57-4016-e811-9c77-1c1b0d1151db', N'.NET'),
    ('8969be57-4016-e811-9c77-1c1b0d1151db', N'Java'),
    ('8a69be57-4016-e811-9c77-1c1b0d1151db', N'Front End'),
    ('8b69be57-4016-e811-9c77-1c1b0d1151db', N'Programming Languages and Principles'),
    ('8c69be57-4016-e811-9c77-1c1b0d1151db', N'Foundations of Software Development'),
    ('8d69be57-4016-e811-9c77-1c1b0d1151db', N'Mobile'),
    ('8e69be57-4016-e811-9c77-1c1b0d1151db', N'Special Trainings'),
    ('8f69be57-4016-e811-9c77-1c1b0d1151db', N'QA Manual'),
    ('9069be57-4016-e811-9c77-1c1b0d1151db', N'QA Automation'),
    ('9169be57-4016-e811-9c77-1c1b0d1151db', N'Business Analysis'),
    ('9269be57-4016-e811-9c77-1c1b0d1151db', N'Databases, Business Intelligence and Big Data'),
    ('9369be57-4016-e811-9c77-1c1b0d1151db', N'DevOps'),
    ('9469be57-4016-e811-9c77-1c1b0d1151db', N'Project Management'),
    ('9569be57-4016-e811-9c77-1c1b0d1151db', N'Sales');

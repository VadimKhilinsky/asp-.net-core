﻿INSERT INTO [Catalog].[TrainerGroup]([Id], [Name])
VALUES
    ('0290c01b-4a16-e811-9c77-1c1b0d1151db', N'Training Department'),
    ('0390c01b-4a16-e811-9c77-1c1b0d1151db', N'.Net'),
    ('0490c01b-4a16-e811-9c77-1c1b0d1151db', N'Java'),
    ('0590c01b-4a16-e811-9c77-1c1b0d1151db', N'Front End & Web'),
    ('0690c01b-4a16-e811-9c77-1c1b0d1151db', N'Mobile'),
    ('0790c01b-4a16-e811-9c77-1c1b0d1151db', N'Quality Assurance'),
    ('0890c01b-4a16-e811-9c77-1c1b0d1151db', N'QA Automation'),
    ('0990c01b-4a16-e811-9c77-1c1b0d1151db', N'Business Analysis'),
    ('0a90c01b-4a16-e811-9c77-1c1b0d1151db', N'Business Intelligence'),
    ('0b90c01b-4a16-e811-9c77-1c1b0d1151db', N'DevOps'),
    ('0c90c01b-4a16-e811-9c77-1c1b0d1151db', N'Project Management');

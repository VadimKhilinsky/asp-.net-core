﻿using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface IRepository<TEntity> : IRepository where TEntity : Entity
    {
        TEntity Find(ISpecification<TEntity> specification);
        IEnumerable<TEntity> List(ISpecification<TEntity> specification = null);

        void Add(TEntity entity);
        void Remove(TEntity entity);
    }
}
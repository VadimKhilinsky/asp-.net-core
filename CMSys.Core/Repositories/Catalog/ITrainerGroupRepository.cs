﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Core.Repositories.Catalog
{
    public interface ITrainerGroupRepository : IRepository<TrainerGroup, Guid>
    {
        Dictionary<string, int> Dependents(Guid id);
    }
}
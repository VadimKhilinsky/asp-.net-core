﻿using CMSys.Common.Paging;
using CMSys.Core.Entities.Catalog;
using System;
using System.Collections.Generic;

namespace CMSys.Core.Repositories.Catalog
{
    public interface ICourseRepository : IRepository<Course, Guid>
    {
        Dictionary<string, int> Dependents(Guid id);
        PagedList<Course> GetPagedList(FilteredPageInfo pageInfo, ISpecification<Course> specification = null);
    }
}

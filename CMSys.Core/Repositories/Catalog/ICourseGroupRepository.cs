﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Core.Repositories.Catalog
{
    public interface ICourseGroupRepository : IRepository<CourseGroup, Guid>
    {
        Dictionary<string, int> Dependents(Guid id);
    }
}
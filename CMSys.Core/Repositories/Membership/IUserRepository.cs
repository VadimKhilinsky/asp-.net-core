﻿using System;
using CMSys.Common.Paging;
using CMSys.Core.Entities.Membership;

namespace CMSys.Core.Repositories.Membership
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        PagedList<User> GetPagedList(FilteredPageInfo pageInfo, ISpecification<User> specification = null);
    }
}
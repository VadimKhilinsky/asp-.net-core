﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ISpecification<TEntity> where TEntity : Entity
    {
        Expression<Func<TEntity, bool>> Criteria { get; }
        IReadOnlyCollection<string> Includes { get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using CMSys.Common.Paging;

namespace CMSys.Core.Entities.Filters
{
    public class UserFilter : Filter
    {
        public string Title { get; set; }

        public UserFilter(string title)
        {
            Title = title;
        }
    }
}

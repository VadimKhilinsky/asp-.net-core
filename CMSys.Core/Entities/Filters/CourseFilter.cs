﻿using CMSys.Common.Paging;
using CMSys.Core.Entities.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMSys.Core.Entities.Filters
{
    public class CourseFilter : Filter
    {
        public Arrangement? Arrangement { get; set; }
        public CourseType? CourseType { get; set; }
        public bool IsNew { get; set; }

        public CourseFilter(Arrangement? arrangement, CourseType? courseType, bool isNew)
        {
            Arrangement = arrangement;
            CourseType = courseType;
            IsNew = isNew;
        }
    }
}

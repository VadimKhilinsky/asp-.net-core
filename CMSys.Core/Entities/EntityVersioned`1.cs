﻿namespace CMSys.Core.Entities
{
    public class EntityVersioned<TKey> : Entity<TKey>
    {
        public byte[] Version { get; }
    }
}
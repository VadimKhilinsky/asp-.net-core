﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities.Catalog
{
    public sealed class Course : EntityVersioned<Guid>
    {
        public const int TitleLength = 64;
        public const int InfoLength = 4000;

        public string Title { get; set; }
        public string Info { get; set; }
        public bool IsNew { get; set; }
        public CourseType CourseType{ get; set; }
        public Arrangement Arrangement { get; set; }
        public Guid GroupId { get; set; }

        public CourseGroup CourseGroup { get; }
    }
}

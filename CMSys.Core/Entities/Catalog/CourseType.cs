﻿using CMSys.Common.Attributes;

namespace CMSys.Core.Entities.Catalog
{
    public enum CourseType
    {
        [StringValue("Practice")]
        Practice,

        [StringValue("Lectures")]
        Lectures
    }
}
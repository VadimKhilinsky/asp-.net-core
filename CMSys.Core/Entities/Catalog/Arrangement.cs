﻿using CMSys.Common.Attributes;

namespace CMSys.Core.Entities.Catalog
{
    public enum Arrangement
    {
        [StringValue("On Demand")]
        OnDemand,

        [StringValue("Scheduled")]
        Scheduled
    }
}
﻿using System;
using CMSys.Common;

namespace CMSys.Core.Entities.Catalog
{
    public sealed class TrainerGroup : EntityVersioned<Guid>
    {
        public const int NameLength = 32;

        public string Name { get; set; }

        private TrainerGroup()
        {
        }

        public static TrainerGroup Create(string name)
        {
            return new TrainerGroup
            {
                Id = SqlServerFriendlyGuid.Generate(),
                Name = name
            };
        }
    }
}
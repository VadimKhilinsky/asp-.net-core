﻿using System;
using CMSys.Common;

namespace CMSys.Core.Entities.Catalog
{
    public sealed class CourseGroup : EntityVersioned<Guid>
    {
        public const int TitleLength = 64;

        public string Title { get; set; }

        private CourseGroup()
        {
        }

        public static CourseGroup Create(string title)
        {
            return new CourseGroup
            {
                Id = SqlServerFriendlyGuid.Generate(),
                Title = title
            };
        }
    }
}
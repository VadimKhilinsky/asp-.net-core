﻿using System;
using CMSys.Core.Entities.Membership;

namespace CMSys.Core.Entities.Catalog
{
    public sealed class Trainer : EntityVersioned<Guid>
    {
        public const int InfoLength = 4000;

        public string Info { get; set; }
        public Guid TrainerGroupId { get; set; }

        public User User { get; }
        public TrainerGroup TrainerGroup { get; }
    }
}
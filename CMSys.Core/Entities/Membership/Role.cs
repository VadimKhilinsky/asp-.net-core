﻿namespace CMSys.Core.Entities.Membership
{
    public sealed class Role : EntityVersioned<int>
    {
        public const int NameLength = 32;

        public string Name { get; set; }

        private Role()
        {
        }
    }
}
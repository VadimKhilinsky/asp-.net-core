﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMSys.Common.Helpers;

namespace CMSys.Core.Entities.Membership
{
    public sealed class User : EntityVersioned<Guid>
    {
        public const int EmailLength = 128;
        public const int PasswordHashLength = 128;
        public const int PasswordSaltLength = 128;
        public const int FirstNameLength = 128;
        public const int LastNameLength = 128;
        public const int DepartmentLength = 128;
        public const int PositionLength = 128;
        public const int LocationLength = 128;

        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Location { get; set; }
        public byte[] Photo { get; private set; }

        public ICollection<UserRole> Roles { get; }

        public string FullName => $"{FirstName} {LastName}";

        private User()
        {
            Roles = new HashSet<UserRole>();
        }

        public User SetPhoto(byte[] photo)
        {
            if (Photo == null)
            {
                Photo = photo;
                return this;
            }

            if (!Photo.SequenceEqual(photo))
            {
                Photo = photo;
            }

            return this;
        }

        public bool VerifyPassword(string password)
        {
            return !string.IsNullOrEmpty(password) &&
                   PasswordHelper.ComputeHash(password, PasswordSalt) == PasswordHash;
        }
    }
}
﻿using System;
using System.Linq;
using CMSys.Core.Exceptions;
using CMSys.Core.Repositories;
using CMSys.Core.Repositories.Catalog;
using CMSys.Core.Repositories.Membership;
using CMSys.Data.Repositories.Catalog;
using CMSys.Data.Repositories.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace CMSys.Data
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly DbContextOptions<CMSysContext> _options;
        private CMSysContext _context;

        private CourseRepository _courseRepository;
        private CourseGroupRepository _courseGroupRepository;
        private CourseTrainerRepository _courseTrainerRepository;
        private TrainerGroupRepository _trainerGroupRepository;
        private TrainerRepository _trainerRepository;

        private RoleRepository _roleRepository;
        private UserRepository _userRepository;

        private CMSysContext Context => _context ??= new CMSysContext(_options);

        #region Catalog

        public ICourseRepository CourseRepository =>
            _courseRepository ??= new CourseRepository(Context);

        public ICourseGroupRepository CourseGroupRepository =>
            _courseGroupRepository ??= new CourseGroupRepository(Context);

        public ICourseTrainerRepository CourseTrainerRepository =>
            _courseTrainerRepository ??= new CourseTrainerRepository(Context);

        public ITrainerGroupRepository TrainerGroupRepository =>
            _trainerGroupRepository ??= new TrainerGroupRepository(Context);

        public ITrainerRepository TrainerRepository => _trainerRepository ??= new TrainerRepository(Context);

        #endregion

        #region Membership

        public IRoleRepository RoleRepository => _roleRepository ??= new RoleRepository(Context);
        public IUserRepository UserRepository => _userRepository ??= new UserRepository(Context);

        #endregion

        public UnitOfWork(IOptions<UnitOfWorkOptions> optionsAccessor) : this(optionsAccessor.Value)
        {
        }

        public UnitOfWork(UnitOfWorkOptions options)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CMSysContext>();
            optionsBuilder.UseSqlServer(options.ConnectionString, x => x.CommandTimeout(options.CommandTimeout));
            _options = optionsBuilder.Options;
        }

        public void Commit()
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("UnitOfWork");
            }

            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new ConcurrencyException(ex.Entries.Select(x => x.Entity.ToString()));
            }
            catch (DbUpdateException ex)
            {
                throw new UpdateException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Commit error.", ex);
            }
        }

        private bool _isDisposed;

        public void Dispose()
        {
            if (_context == null)
            {
                return;
            }

            if (!_isDisposed)
            {
                Context.Dispose();
            }

            _isDisposed = true;
        }
    }
}
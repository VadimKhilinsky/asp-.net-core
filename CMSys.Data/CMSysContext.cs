﻿using System;
using System.Collections.Generic;
using System.Data;
using CMSys.Common;
using CMSys.Data.Configurations.Catalog;
using CMSys.Data.Configurations.Membership;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Data
{
    internal class CMSysContext : DbContext
    {
        public CMSysContext(DbContextOptions<CMSysContext> options) : base(options)
        {
        }

        public Dictionary<string, int> FindDependents(string schema, string table, Guid id)
        {
            Expect.ArgumentNotNull(schema, nameof(schema));
            Expect.ArgumentNotNull(table, nameof(table));
            var schemaParam = new SqlParameter("@Schema", SqlDbType.NVarChar, 50) {Value = schema};
            var tableParam = new SqlParameter("@Table", SqlDbType.NVarChar, 50) {Value = table};
            var idParam = new SqlParameter("@Id", SqlDbType.NVarChar, 50) {Value = $"N'{id}'"};
            var result = new Dictionary<string, int>(100);
            using (var command = Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "FindDependents";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(schemaParam);
                command.Parameters.Add(tableParam);
                command.Parameters.Add(idParam);
                if (Database.GetDbConnection().State != ConnectionState.Open)
                {
                    Database.OpenConnection();
                }

                using var reader = command.ExecuteReader(CommandBehavior.SingleResult);
                while (reader.Read())
                {
                    // Schema Table Count -> 0 1 2
                    var dependenciesCount = reader.GetFieldValue<int>(2);
                    if (dependenciesCount > 0)
                    {
                        var dependentTable = reader.GetFieldValue<string>(1);
                        result.Add(dependentTable, dependenciesCount);
                    }
                }
            }

            return result;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CourseConfiguration());
            modelBuilder.ApplyConfiguration(new CourseGroupConfiguration());
            modelBuilder.ApplyConfiguration(new TrainerConfiguration());
            modelBuilder.ApplyConfiguration(new TrainerGroupConfiguration());
            modelBuilder.ApplyConfiguration(new CourseTrainerConfiguration());

            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
        }
    }
}
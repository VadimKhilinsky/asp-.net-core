﻿using CMSys.Core.Entities.Membership;
using CMSys.Core.Repositories.Membership;

namespace CMSys.Data.Repositories.Membership
{
    internal sealed class RoleRepository : Repository<Role, int>, IRoleRepository
    {
        public RoleRepository(CMSysContext context) : base(context)
        {
        }
    }
}
﻿using System;
using System.Linq;
using CMSys.Common.Paging;
using CMSys.Core.Entities.Filters;
using CMSys.Core.Entities.Membership;
using CMSys.Core.Exceptions;
using CMSys.Core.Repositories;
using CMSys.Core.Repositories.Membership;
using CMSys.Data.Helpers;
using Microsoft.Data.SqlClient;

namespace CMSys.Data.Repositories.Membership
{
    internal sealed class UserRepository : Repository<User, Guid>, IUserRepository
    {
        public UserRepository(CMSysContext context) : base(context)
        {
        }

        public PagedList<User> GetPagedList(FilteredPageInfo pageInfo, ISpecification<User> specification = null)
        {
            try
            {
                var query = Apply(specification);
                query = ApplyFilter(query, (UserFilter)pageInfo.Filter);
                var items = query.SelectPage(pageInfo).ToList();
                var total = query.Count();

                return new PagedList<User>(items, total, pageInfo);
            }
            catch (SqlException ex)
            {
                throw ex.ToRepositoryException();
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        private static IQueryable<User> ApplyFilter(IQueryable<User> query, UserFilter filter)
        {
            query = query.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);

            if (string.IsNullOrEmpty(filter.Title))
            {
                return query;
            }

            var title = filter.Title.ToUpper();
            query = title.Contains(" ")
                ? query.AsEnumerable().Where(x => x.FullName.ToUpper().Contains(title)).AsQueryable()
                : query.AsEnumerable().Where(x => x.FirstName.ToUpper().Contains(title) || x.LastName.ToUpper().Contains(title)).AsQueryable();

            return query;
        }
    }
}
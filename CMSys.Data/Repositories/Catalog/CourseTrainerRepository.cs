﻿using System;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories;
using CMSys.Core.Repositories.Catalog;

namespace CMSys.Data.Repositories.Catalog
{
    internal sealed class CourseTrainerRepository : Repository<CourseTrainer>, ICourseTrainerRepository
    {
        public CourseTrainerRepository(CMSysContext context) : base(context)
        {
        }

        public CourseTrainer Find(Guid courseId, Guid trainerId)
        {
            return Find(new Specification<CourseTrainer>(x => x.CourseId == courseId && x.TrainerId == trainerId));
        }
    }
}
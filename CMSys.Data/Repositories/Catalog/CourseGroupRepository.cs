﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Repositories.Catalog;
using CMSys.Data.Configurations;

namespace CMSys.Data.Repositories.Catalog
{
    internal sealed class CourseGroupRepository : Repository<CourseGroup, Guid>, ICourseGroupRepository
    {
        public CourseGroupRepository(CMSysContext context) : base(context)
        {
        }

        public Dictionary<string, int> Dependents(Guid id)
        {
            return Context.FindDependents(Schema.Catalog, nameof(CourseGroup), id);
        }
    }
}
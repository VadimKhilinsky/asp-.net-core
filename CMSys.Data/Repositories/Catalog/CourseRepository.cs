﻿using CMSys.Common.Helpers;
using CMSys.Common.Paging;
using CMSys.Core.Entities.Catalog;
using CMSys.Core.Entities.Filters;
using CMSys.Core.Exceptions;
using CMSys.Core.Repositories;
using CMSys.Core.Repositories.Catalog;
using CMSys.Data.Configurations;
using CMSys.Data.Helpers;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMSys.Data.Repositories.Catalog
{
    internal sealed class CourseRepository : Repository<Course, Guid>, ICourseRepository
    {
        public CourseRepository(CMSysContext context) : base(context)
        {
        }

        public Dictionary<string, int> Dependents(Guid id)
        {
            return Context.FindDependents(Schema.Catalog, nameof(Course), id);
        }

        public PagedList<Course> GetPagedList(FilteredPageInfo pageInfo, ISpecification<Course> specification = null)
        {
            try
            {
                var query = Apply(specification);
                query = ApplyFilter(query, (CourseFilter)pageInfo.Filter);
                var items = query.SelectPage(pageInfo).ToList();
                var total = query.Count();

                return new PagedList<Course>(items, total, pageInfo);
            }
            catch (SqlException ex)
            {
                throw ex.ToRepositoryException();
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }

        private static IQueryable<Course> ApplyFilter(IQueryable<Course> query, CourseFilter filter)
        {
            query = query.OrderBy(x => x.Title);

            if (filter is null)
            {
                return query;
            }

            if(filter.Arrangement.HasValue)
            {
                query = query.Where(course => course.Arrangement.Equals(filter.Arrangement));
            }

            if(filter.CourseType.HasValue)
            {
                query = query.Where(course => course.CourseType.Equals(filter.CourseType));
            }

            if(filter.IsNew == true)
            {
                query = query.Where(course => course.IsNew == true);
            }

            return query;
        }
    }
}

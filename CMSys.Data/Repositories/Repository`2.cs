﻿using System;
using CMSys.Core.Entities;
using CMSys.Core.Exceptions;
using CMSys.Core.Repositories;
using CMSys.Data.Helpers;
using Microsoft.Data.SqlClient;

namespace CMSys.Data.Repositories
{
    internal abstract class Repository<TEntity, TKey> : Repository<TEntity>, IRepository<TEntity, TKey>
        where TEntity : Entity<TKey>
    {
        protected Repository(CMSysContext context) : base(context)
        {
        }

        public virtual TEntity Find(TKey id)
        {
            try
            {
                return DbSet.Find(id);
            }
            catch (SqlException ex)
            {
                throw ex.ToRepositoryException();
            }
            catch (Exception ex)
            {
                throw new RepositoryException(ex.Message, ex);
            }
        }
    }
}
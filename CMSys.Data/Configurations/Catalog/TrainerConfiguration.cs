﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Data.Configurations.Catalog
{
    internal sealed class TrainerConfiguration : IEntityTypeConfiguration<Trainer>
    {
        public void Configure(EntityTypeBuilder<Trainer> builder)
        {
            builder.ToTable(nameof(Trainer), Schema.Catalog);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedNever();
            builder.Property(x => x.Info).HasMaxLength(Trainer.InfoLength);
            builder.Property(x => x.TrainerGroupId);

            builder.HasOne(x => x.User).WithMany().HasForeignKey(x => x.Id);
            builder.HasOne(x => x.TrainerGroup).WithMany().HasForeignKey(x => x.TrainerGroupId);

            builder.Property(x => x.Version).ValueGeneratedOnAddOrUpdate().IsConcurrencyToken();
        }
    }
}
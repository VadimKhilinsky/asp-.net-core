﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Data.Configurations.Catalog
{
    internal sealed class CourseGroupConfiguration : IEntityTypeConfiguration<CourseGroup>
    {
        public void Configure(EntityTypeBuilder<CourseGroup> builder)
        {
            builder.ToTable(nameof(CourseGroup), Schema.Catalog);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).IsRequired().HasMaxLength(CourseGroup.TitleLength);

            builder.Property(p => p.Version).ValueGeneratedOnAddOrUpdate().IsConcurrencyToken();
        }
    }
}
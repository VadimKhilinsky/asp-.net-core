﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Data.Configurations.Catalog
{
    internal sealed class CourseTrainerConfiguration : IEntityTypeConfiguration<CourseTrainer>
    {
        public void Configure(EntityTypeBuilder<CourseTrainer> entity)
        {
            entity.ToTable(nameof(CourseTrainer), Schema.Catalog);
            entity.HasKey(x => new {x.CourseId, x.TrainerId});

            entity.HasOne(x => x.Trainer).WithMany().HasForeignKey(x => x.TrainerId);
            entity.HasOne(x => x.Course).WithMany().HasForeignKey(x => x.CourseId);
        }
    }
}
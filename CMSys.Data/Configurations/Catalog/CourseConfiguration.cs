﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Data.Configurations.Catalog
{
    internal sealed class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.ToTable(nameof(Course), Schema.Catalog);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Info).HasMaxLength(Course.InfoLength);
            builder.Property(x => x.Title).IsRequired().HasMaxLength(Course.TitleLength);
            builder.Property(x => x.IsNew).IsRequired();
            builder.Property(x => x.CourseType).IsRequired();
            builder.Property(x => x.Arrangement).IsRequired();

            builder.HasOne(x => x.CourseGroup).WithMany().HasForeignKey(x => x.GroupId);

            builder.Property(x => x.Version).ValueGeneratedOnAddOrUpdate().IsConcurrencyToken();
        }
    }
}

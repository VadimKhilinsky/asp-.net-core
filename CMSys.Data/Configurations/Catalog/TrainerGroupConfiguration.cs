﻿using CMSys.Core.Entities.Catalog;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Data.Configurations.Catalog
{
    internal sealed class TrainerGroupConfiguration : IEntityTypeConfiguration<TrainerGroup>
    {
        public void Configure(EntityTypeBuilder<TrainerGroup> builder)
        {
            builder.ToTable(nameof(TrainerGroup), Schema.Catalog);

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(TrainerGroup.NameLength);
            builder.Property(p => p.Version).ValueGeneratedOnAddOrUpdate().IsConcurrencyToken();
        }
    }
}
﻿using CMSys.Core.Entities.Membership;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Data.Configurations.Membership
{
    internal sealed class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User), Schema.Membership);

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Email).IsRequired().HasMaxLength(User.EmailLength);
            builder.Property(x => x.PasswordHash).IsRequired().HasMaxLength(User.PasswordHashLength);
            builder.Property(x => x.PasswordSalt).IsRequired().HasMaxLength(User.PasswordSaltLength);
            builder.Property(x => x.FirstName).IsRequired().HasMaxLength(User.FirstNameLength);
            builder.Property(x => x.LastName).IsRequired().HasMaxLength(User.LastNameLength);
            builder.Property(x => x.StartDate).IsRequired().HasColumnType("date");
            builder.Property(x => x.EndDate).HasColumnType("date");
            builder.Property(x => x.Department).HasMaxLength(User.DepartmentLength);
            builder.Property(x => x.Position).HasMaxLength(User.PositionLength);
            builder.Property(x => x.Location).HasMaxLength(User.LocationLength);
            builder.Property(x => x.Photo);

            builder.HasMany(x => x.Roles).WithOne().HasForeignKey(x => x.UserId);

            builder.Property(x => x.Version).ValueGeneratedOnAddOrUpdate().IsConcurrencyToken();
        }
    }
}
﻿namespace CMSys.Data.Configurations
{
    internal static class Schema
    {
        internal const string Catalog = "Catalog";
        internal const string Membership = "Membership";
    }
}
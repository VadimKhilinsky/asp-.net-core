﻿using System;
using System.Linq;
using System.Reflection;
using CMSys.Common.Attributes;

namespace CMSys.Common.Helpers
{
    public static class EnumHelper
    {
        public static string GetStringValue<TEnum>(this TEnum value) where TEnum : Enum
        {
            var attribute = typeof(TEnum).GetField(value.ToString())?.GetCustomAttributes<StringValueAttribute>()
                .FirstOrDefault();
            return attribute?.Text ?? value.ToString();
        }
    }
}
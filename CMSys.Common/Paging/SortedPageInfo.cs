﻿namespace CMSys.Common.Paging
{
    public class SortedPageInfo : PageInfo
    {
        public string Sort { get; }

        public SortedPageInfo(string sort, int page = 1, int perPage = 10) : base(page, perPage)
        {
            Sort = string.IsNullOrWhiteSpace(sort) ? string.Empty : sort.ToUpper();
        }
    }
}
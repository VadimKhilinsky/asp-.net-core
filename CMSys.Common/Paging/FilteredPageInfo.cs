﻿namespace CMSys.Common.Paging
{
    public class FilteredPageInfo : PageInfo
    {
        public Filter Filter { get; }

        public FilteredPageInfo(Filter filter, int page = 1, int perPage = int.MaxValue) : base(page, perPage)
        {
            Filter = filter;
        }
    }
}
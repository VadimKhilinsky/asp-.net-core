﻿namespace CMSys.Common.Paging
{
    public class FilteredSortedPageInfo : FilteredPageInfo
    {
        public string Sort { get; }

        public FilteredSortedPageInfo(Filter filter, string sort, int page = 1, int perPage = 10) : base(filter, page,
            perPage)
        {
            Sort = string.IsNullOrWhiteSpace(sort) ? string.Empty : sort.ToUpper();
        }
    }
}
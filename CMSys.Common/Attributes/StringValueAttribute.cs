﻿using System;

namespace CMSys.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class StringValueAttribute : Attribute
    {
        public string Text { get; }

        public StringValueAttribute(string text)
        {
            Text = string.IsNullOrWhiteSpace(text) ? string.Empty : text;
        }
    }
}